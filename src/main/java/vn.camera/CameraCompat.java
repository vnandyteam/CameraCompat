package vn.camera;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Build;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 31/10/2016.
 *
 * Pattern Design : Repository Pattern
 */

public class CameraCompat {

    public static final int DEVICE_VERSION          = 1 << 1;
    public static final int V1                      = 1 << 2;
    public static final int V2                      = 1 << 3;

    private static CameraCompat mInstance;
    private ICamera mICamera;

    public static CameraCompat getInstance() {
        if (mInstance == null) {
            mInstance = new CameraCompat();
        }

        return mInstance;
    }

    public CameraCompat setConfig(Context pContext) {
        //Todo: set something config for camera
        mICamera.setConfig(pContext);
        return mInstance;
    }

    /**
     * If Version OS device >= LOLLIPOP use camera v2
     * else use camera V1
     * */
    public CameraCompat Builder() {
        return Builder(DEVICE_VERSION);
    }

    /**
     * Use Camera Vx
     * @param pCameraVersion == #DEVICE_VERSION check android version
     * pCameraVersion == #V1 use Camera V1
     * pCameraVersion == #V2 use Camera V2
     * */
    public CameraCompat Builder(int pCameraVersion) {
        if((pCameraVersion & V1) == V1) { // only use camera v1
            mICamera = new CameraV1(); // deprecate camera v1
        } else if((pCameraVersion & V2) == V2) { // only use camera v2
            mICamera = new CameraV2();
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            mICamera = new CameraV2();
        } else {
            mICamera = new CameraV1();
        }

        return mInstance;
    }

    public CameraCompat setTextureView(AutoFitTextureView pTextureView) {
        mICamera.setTextureView(pTextureView);
        return mInstance;
    }

    /**
     * take
     * */
    public void doTakePhoto() {
        if (mICamera != null) mICamera.doTakePhoto();
    }

    public void doRecordVideo() {
        if (mICamera != null) mICamera.doRecordVideo();
    }

    public void onDestroy() {
        if (mICamera != null) mICamera.onDestroy();
    }

    public void onResume() {
        if (mICamera != null) mICamera.onResume();
    }

    public void onPause() {
        if (mICamera != null) mICamera.onPause();
    }

    public CameraCompat open() {
        if (mICamera != null) mICamera.open();
        return mInstance;
    }

    public CameraCompat setDataCallback(CameraCallback.DataCallback pDataCallback) {
        if (mICamera != null) mICamera.setDataCallback(pDataCallback);
        return this;
    }

    public void setSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
        if (mICamera != null) mICamera.setSurfaceTextureAvailable(surfaceTexture, width, height);
    }
    public void setSurfaceTextureDestroyed(SurfaceTexture surfaceTexture){
        if (mICamera != null) mICamera.setSurfaceTextureDestroyed(surfaceTexture);
    }
    public void setSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height){
        if (mICamera != null) mICamera.setSurfaceTextureSizeChanged(surfaceTexture, width, height);
    }
    public void setSurfaceTextureUpdated(SurfaceTexture surfaceTexture){
        if (mICamera != null) mICamera.setSurfaceTextureUpdated(surfaceTexture);
    }
}
